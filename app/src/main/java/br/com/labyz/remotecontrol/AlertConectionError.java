package br.com.labyz.remotecontrol;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;

import br.com.labyz.remotecontrol.R;
/**
 * Created by IT Staff on 30/05/2018.
 */

public class AlertConectionError {

    private Button btnVoltar;
    private Context context;
    private Dialog dialog;





    public AlertConectionError(Context context) {
        this.context = context;
        init();
    }



    private void init(){
        dialog = new Dialog(context);
        dialog.getWindow().requestFeature(1);
        dialog.setContentView(R.layout.alert_conection_error);

        btnVoltar = dialog.findViewById(R.id.btnVoltar);
        dialog.setCancelable(false);
    }



    public void show(){
        dialog.show();
    }



    public void dismiss(){
        dialog.dismiss();
    }



    public void setOnBackListener(View.OnClickListener listener){
        btnVoltar.setOnClickListener(listener);
    }

}
