package br.com.labyz.remotecontrol;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

/**
 * Created by IT Staff on 21/05/2018.
 *
 * Classe para o joystick quadrado do controle 1.
 */

public class JoystickView extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener {

    private float centerX;
    private float centerY;
    private float baseRadius;
    private float hatRadius;

    private float rWidth;
    private float rHeight;
    private float rLeft;
    private float rTop;
    private float rBottom;
    private float rRight;

    private JoystickListener listener;

    public float getJWidth() {
        return rWidth;
    }

    public float getJHeight() {
        return rHeight;
    }

    public JoystickView(Context context) {
        super(context);
        getHolder().addCallback(this);
        setOnTouchListener(this);

        if (context instanceof JoystickListener){
            listener = (JoystickListener) context;
        }
    }



    public JoystickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        setOnTouchListener(this);

        if (context instanceof JoystickListener){
            listener = (JoystickListener) context;
        }
    }



    public JoystickView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getHolder().addCallback(this);
        setOnTouchListener(this);

        if (context instanceof JoystickListener){
            listener = (JoystickListener) context;
        }
    }



    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i("Canvas","surfaceCreated");
        setupDimensions();
        drawJoystick(centerX,centerY);
    }



    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i("Canvas","surfaceChanged");

    }



    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i("Canvas","surfaceDestroyed");

    }


    /**
     * Função que desenha o joystick em um canvas.
     * @param newX nova posição X para o stick do joystick.
     * @param newY nova posição Y para o stick do joystick.
     */
    private void drawJoystick(float newX, float newY){
        Log.i("Canvas","drawJoystick");
        if (getHolder().getSurface().isValid()){
            Log.i("Canvas","isValid");
            Canvas canvas = getHolder().lockCanvas();
            Paint colors = new Paint();

            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

            //Base
            colors.setARGB(255,42,42,42);
            canvas.drawRect(rLeft,rTop,rRight,rBottom, colors);

            colors.setARGB(255,255,100,100);
            float mark1 = Math.min(getWidth(), getHeight()) / 4;
            canvas.drawCircle(centerX,centerY,mark1,colors);
            colors.setARGB(255,42,42,42);
            canvas.drawCircle(centerX,centerY,mark1 - 5,colors);

            colors.setARGB(255,255,100,100);
            canvas.drawLine(rWidth / 2, 0.0f, rWidth / 2, getHeight(), colors);
            canvas.drawLine(0.0f, rHeight / 2, rWidth, rHeight/2, colors);

            //Stick
            colors.setARGB(255,180,180,180);
            canvas.drawCircle(newX,newY,hatRadius,colors);

            //Bordas Brancas
            colors.setARGB(255,255,255,255);
            colors.setStrokeWidth(15);
            canvas.drawLine(0.0f, 0.0f, rWidth, 0.0f, colors); //Top
            canvas.drawLine(0.0f, 0.0f, 0.0f, rHeight, colors); //Left
            canvas.drawLine(rWidth, 0.0f, rWidth, rHeight, colors); //Right
            canvas.drawLine(0.0f, rHeight, rWidth, rHeight, colors); //Bottom

            //Desenha o canvas
            getHolder().unlockCanvasAndPost(canvas);
        }
    }



    private void setupDimensions() {
        Log.i("Canvas", "setupDimensions");

        rWidth  = getWidth();
        rLeft   = 0;
        rRight  = rLeft+rWidth;
        rTop    = 0;
        rBottom = rTop+rWidth;
        rHeight = getHeight();


        centerX = getWidth() / 2;
        centerY = getHeight() / 2;
        baseRadius = Math.min(getWidth(), getHeight()) / 3;
        hatRadius = Math.min(getWidth(), getHeight()) / 7;

        Log.i("Canvas","Hat Radius: ["+hatRadius+"]");
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.equals(this)){
            if (event.getAction() != MotionEvent.ACTION_UP){

                float touchX = event.getX();
                float touchY = event.getY();

                Log.i("Canvas", "Touch X ["+touchX+"]  Touch Y["+touchY+"]");
                Log.i("Canvas", "Get X ["+getX()+"]  Get Y["+getY()+"]  Get Width["+getWidth()+"]  Get Height["+getHeight()+"]");


                //Não permite que o stick avança as bordas do canvas e desapareça se o usuário manter
                //pressionado até sair do joystick
                if ((touchX <= 0) && (touchY <= 0)){
                    drawJoystick(0,0);
                    listener.onDrag(- getWidth()/2,- getHeight()/2, getWidth(), getHeight());
                }else if ((touchX >= getWidth()) && (touchY <= 0)){
                    drawJoystick(getWidth(),0);
                    listener.onDrag(getWidth()/2,- getHeight()/2, getWidth(), getHeight());
                }else if ((touchX >= getWidth()) && (touchY >= getHeight())){
                    drawJoystick(getWidth(),getHeight());
                    listener.onDrag(getWidth()/2,getHeight()/2, getWidth(), getHeight());
                }else if ((touchX <= 0) && (touchY >= getHeight())){
                    drawJoystick(0,getHeight());
                    listener.onDrag(- getWidth()/2,getHeight()/2, getWidth(), getHeight());
                }else if(touchY < 0){   //Acima
                    drawJoystick(touchX,0);
                    listener.onDrag(touchX - (getWidth()/2),- getHeight()/2, getWidth(), getHeight());
                }else if(touchY > getHeight()){ //Abaixo
                    drawJoystick(touchX,getHeight());
                    listener.onDrag(touchX - (getWidth()/2),getHeight() / 2, getWidth(), getHeight());
                }else if (touchX < 0){ //Esquerda
                    drawJoystick(0,touchY);
                    listener.onDrag(- getWidth()/2,touchY - (getHeight() / 2), getWidth(), getHeight());
                }else  if (touchX > getWidth()){ //Direita
                    drawJoystick(getWidth(), touchY);
                    listener.onDrag(getWidth() / 2,touchY - (getHeight() / 2), getWidth(), getHeight());
                }else {
                    drawJoystick(touchX,touchY);
                    listener.onDrag(touchX - (getWidth()/2),touchY - (getHeight() / 2), getWidth(), getHeight());
                }



            }else {
                drawJoystick(centerX,centerY);
                listener.onUp();
            }
        }

        return true;

        /*if (touchX < rLeft && touchX > rRight) return true;
                if (touchX < rLeft && touchX > rRight) return true;

                drawJoystick(event.getX(),event.getY());*/
    }
}
