package br.com.labyz.remotecontrol;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by IT Staff on 17/05/2018.
 *
 * Realiza a comunicação com o módulo Bluetooth.
 */

public class BluetoothComm {

    private InputStream is;
    private OutputStream os;
    private final UUID DEFAULT_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothDevice device;
    private BluetoothSocket socket;
    private SendThread sendThread;
    private ReadThread readThread;

    private OnBluetoothReadListener btListener;


    /**
     * Construtor
     * @param device dispositivo bluetooth selecionado para conexão.
     * @param listener listener para enviar as informações recebidas do sonar para outra classe.
     */
    public BluetoothComm(BluetoothDevice device, OnBluetoothReadListener listener) {
        super();
        this.device = device;
        this.btListener = listener;
    }



    public void connect() throws IOException {
        socket = device.createInsecureRfcommSocketToServiceRecord(DEFAULT_UUID);
        socket.connect();

        is = socket.getInputStream();
        os = socket.getOutputStream();
        sendThread = new SendThread().startThread();
        readThread = new ReadThread().startThread();
    }



    public void disconnect() throws IOException {
        socket.close();
        sendThread.finish();
        readThread.finish();
    }


    public void send(String s) {
        sendThread.put(s);
    }

    public void sendBytes(String s) throws IOException {
        os.write(s.getBytes());
        os.flush();
    }


    /**
     * Thread para enviar dados.
     * Possui uma estrutura de fila para guardar e enviar os comandos conforme forem sendo gerados no joystick.
     * Garante que os comandos serão enviados ordenadamente.
     */
    public class SendThread extends Thread {
        protected ArrayList<String> queue;
        protected boolean isRunning = false;

        public SendThread() {
            queue = new ArrayList<>();
        }

        public SendThread startThread() {
            this.start();
            return this;
        }

        public void finish() {
            isRunning = false;
            this.interrupt();
        }

        public void put(String cmd) {
            synchronized (queue) {
                queue.add(cmd);
                queue.notifyAll();
            }
        }




        public void run() {
            String cmd = null;
            isRunning = true;

            while(isRunning) {

                synchronized (queue) {
                    if (queue.size() > 0) {
                        cmd = queue.get(0);
                        queue.remove(0);
                    } else {
                        cmd = null;
                        try { queue.wait(); } catch (InterruptedException ie) {}
                    }
                }

                if (cmd != null) {
                    try {
                        Log.w("BluetoothComm","SendThread Send ["+cmd+"]");
                        sendBytes(cmd);
                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            }
            Log.w("BluetoothComm","SendThread Stopped");
        }
    }


    /**
     * Thread para receber dados.
     *
     */
    public class ReadThread extends Thread {
        protected boolean isRunning = false;
        protected States state = States.ST_CMD; //Inicia a máquina de estados aguardando um comando

        public ReadThread() {

        }

        public ReadThread startThread() {
            this.start();
            return this;
        }

        public void finish() {
            isRunning = false;
            this.interrupt();
        }



        public void run() {
            isRunning = true;
            char ch;
            String cmd = "";
            int    b    =0;

            while(isRunning) {
                try{
                    b = is.read();

                    if (b > 0) {
                        ch = (char) b;
                        switch (state) {
                            case ST_CMD:
                                if (ch != ',') { //Enquanto não receber um caractere ",", vai concatenando até formar uma parte do comando
                                    cmd += ch;
                                } else {
                                    switch (cmd) { //Verifica o comando recebido
                                        case "SONAR":
                                            state = States.ST_SONAR; //Ao receber um comando "SONAR", muda o estado da máquina de estados para ST_SONAR
                                            cmd = ""; //Limpa a variável para receber o resto do comando
                                            break;
                                    }
                                }
                                break;

                            case ST_SONAR:
                                if (ch == '\n') { //Ao receber um "\n"
                                    Log.w("BluetoothComm", "CMD SONAR: [" + cmd + "]");
                                    btListener.onSonarRead(cmd); //Envia o comando para o listener
                                    cmd = ""; //Limpa a variável
                                    state = States.ST_CMD; //Muda o estado da máquina de estados para ST_CMD para aguardar novo comando
                                } else {
                                    cmd += ch;
                                }
                                break;

                        }
                    } else {
                        try {Thread.sleep(50);} catch (Throwable t) {t.printStackTrace();}
                    }
                }catch (Throwable t){
                    t.printStackTrace();
                    try {Thread.sleep(1000);} catch (Throwable t2) {t2.printStackTrace();}
                }

            }
            Log.w("BluetoothComm","ReadThread Stopped");
        }
    }



    protected enum States {ST_CMD, ST_ID, ST_SONAR}
}
