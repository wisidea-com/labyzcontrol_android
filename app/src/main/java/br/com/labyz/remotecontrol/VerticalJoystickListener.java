package br.com.labyz.remotecontrol;

/**
 * Created by IT Staff on 28/05/2018.
 */

public interface VerticalJoystickListener {
    void onDragY(float Y, int maxWidth, int maxHeight);
    void onUpY(int maxWidth, int maxHeight);
    void onSetudDimensionsY(int maxWidth, int maxHeight);
}
