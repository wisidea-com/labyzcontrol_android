package br.com.labyz.remotecontrol;

/**
 * Created by IT Staff on 22/05/2018.
 */

public interface JoystickListener {
    void onDrag(float X, float Y, int maxWidth, int maxHeight);
    void onUp();
}
