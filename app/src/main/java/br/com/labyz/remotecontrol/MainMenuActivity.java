package br.com.labyz.remotecontrol;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.labyz.remotecontrol.R;

public class MainMenuActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView lvMenu;
    private ArrayList<String> options;
    private MenuAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        (lvMenu = findViewById(R.id.lvMenu)).setOnItemClickListener(this);

        options = new ArrayList<>();
        options.add("Controle Robô SMARS");
        options.add("Sobre");

        adapter = new MenuAdapter(this,options);
        lvMenu.setAdapter(adapter);
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:
                Intent it = new Intent(MainMenuActivity.this, DevicesActivity.class);
                startActivity(it);
                break;

            case 1:
                Intent it2 = new Intent(MainMenuActivity.this, AboutActivity.class);
                startActivity(it2);
            break;
        }
    }
}