package br.com.labyz.remotecontrol;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.ImageButton;

import br.com.labyz.remotecontrol.R;

/**
 * Created by IT Staff on 30/05/2018.
 *
 * Cria o Alert de seleção do controle.
 */

public class AlertChooseControl {

    private ImageButton btnControle1;
    private ImageButton btnControle2;
    private Context context;
    private Dialog dialog;





    public AlertChooseControl(Context context) {
        this.context = context;
        init();
    }



    private void init(){
        dialog = new Dialog(context);
        dialog.getWindow().requestFeature(1);
        dialog.setContentView(R.layout.alert_choose_control);

        btnControle1 = dialog.findViewById(R.id.btnControle1);
        btnControle2 = dialog.findViewById(R.id.btnControle2);
    }



    public void show(){
        dialog.show();
    }



    public void dismiss(){
        dialog.dismiss();
    }



    public void setControl1Listener(View.OnClickListener listener){
        btnControle1.setOnClickListener(listener);
    }



    public void setControl2Listener(View.OnClickListener listener){
        btnControle2.setOnClickListener(listener);
    }
}

