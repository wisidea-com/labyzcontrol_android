package br.com.labyz.remotecontrol;

/**
 * Created by IT Staff on 06/06/2018.
 */

public interface OnBluetoothReadListener {

    void onSonarRead(String cmd);
}
