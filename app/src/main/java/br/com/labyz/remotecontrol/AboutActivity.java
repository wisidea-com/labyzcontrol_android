package br.com.labyz.remotecontrol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView t;

        t = (TextView) findViewById(R.id.about_explainText);
        t.setText(Html.fromHtml(getString(R.string.about_explain)));
        t.setMovementMethod(LinkMovementMethod.getInstance());

        t = (TextView) findViewById(R.id.about_explainProjects);
        t.setText(Html.fromHtml(getString(R.string.about_explain_projects)));
        t.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
