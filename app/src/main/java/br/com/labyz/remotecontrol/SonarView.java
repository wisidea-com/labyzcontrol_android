package br.com.labyz.remotecontrol;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by IT Staff on 06/06/2018.
 *
 * Classe que desenha o sonar.
 */

public class SonarView extends SurfaceView implements SurfaceHolder.Callback {

    private float centerX;
    private float centerY;

    private float rWidth;
    private float rHeight;
    private float rLeft;
    private float rTop;
    private float rBottom;
    private float rRight;





    public SonarView(Context context) {
        super(context);
        getHolder().addCallback(this);

    }

    public SonarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
    }

    public SonarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i("Sonar","surfaceCreated");
        setupDimensions();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i("Sonar","surfaceChanged");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i("Sonar","surfaceDestroyed");
    }



    private void setupDimensions() {
        Log.i("Canvas", "setupDimensions");

        rWidth  = getWidth();
        rHeight = getHeight();
        rLeft   = 0;
        rRight  = rLeft+rWidth;
        rTop    = 0;
        rBottom = rTop+rHeight;

        Log.i("Sonar", "setupDimensions -> WIDTH: ["+rWidth+"] HEIGHT: ["+rHeight+"]");


        centerX = getWidth() / 2;
        centerY = getHeight() / 2;
    }


    /**
     * Desenha o sonar em um canvas.
     * @param newY posição Y para representar um obstáculo em vermelho conforme vai se aproximando.
     */
    private void drawSonar(float newY){
        Log.i("Canvas","drawJoystick");
        if (getHolder().getSurface().isValid()){
            Log.i("Canvas","isValid");
            Canvas canvas = getHolder().lockCanvas();
            Paint colors = new Paint();

            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

            //Base
            colors.setARGB(255,1,66,3);
            canvas.drawRect(rLeft,rTop,rRight,rBottom, colors);

            //Obstáculo
            colors.setARGB(255,131,0,3);
            canvas.drawRect(rLeft,rTop,rRight, (rTop + newY),colors);

            //Bordas Brancas
            colors.setARGB(255,255,255,255);
            colors.setStrokeWidth(15);
            canvas.drawLine(0.0f, 0.0f, rWidth, 0.0f, colors); //Top
            canvas.drawLine(0.0f, 0.0f, 0.0f, rHeight, colors); //Left
            canvas.drawLine(rWidth, 0.0f, rWidth, rHeight, colors); //Right
            canvas.drawLine(0.0f, rHeight, rWidth, rHeight, colors); //Bottom

            //Desenha o canvas
            getHolder().unlockCanvasAndPost(canvas);
        }
    }



    public void updateDistance(int distance){
        Log.i("Sonar","updateDistance  new distance: ["+distance+"]");
        float invert = rHeight - distance;

        drawSonar(invert);
    }
}
