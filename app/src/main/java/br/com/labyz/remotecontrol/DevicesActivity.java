package br.com.labyz.remotecontrol;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Set;

import br.com.labyz.remotecontrol.R;

public class DevicesActivity extends AppCompatActivity {

    private final String LTAG = getClass().getSimpleName();

    private Button btnSearchDevice;
    private ListView listViewFound;
    private ListView listViewPaired;
    private ProgressBar progressBarSearch;

    private BluetoothDevice btDevice;
    private BluetoothAdapter btAdapter;

    private BroadcastReceiver receiver;

    private ArrayList<String> devicesFoundNames;
    private ArrayAdapter<String> foundAdapter;

    private ArrayList<BluetoothDevice> devicesPairedNames;
    private BTDeviceAdapter pairedAdapter;

    private int REQUEST_CODE_ENABLE_BT = 10;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);

        listViewPaired = findViewById(R.id.listViewPaired);


        btAdapter = BluetoothAdapter.getDefaultAdapter();

        //Paired devices
        devicesPairedNames = new ArrayList<>();
        pairedAdapter = new BTDeviceAdapter(this,devicesPairedNames);
        listViewPaired.setAdapter(pairedAdapter);


        //Pede para ativar o bluetooth
        if (!btAdapter.isEnabled()){
            Intent it = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(it, REQUEST_CODE_ENABLE_BT);
        }

        /*createBroadcastReceiver();

        registerReceiver(receiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        registerReceiver(receiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));*/

        getPairedDevices();


        //Listeners

        listViewPaired.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BluetoothDevice device = devicesPairedNames.get(position);

                //Passa o dispositivo selecionado para outra activity
                final Intent it = new Intent(DevicesActivity.this, MainActivity.class);
                it.putExtra("extra_device", device);

                final AlertChooseControl alert = new AlertChooseControl(DevicesActivity.this);
                alert.setControl1Listener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Passa qual controle a activity deve apresentar
                        it.putExtra("controle",1);
                        startActivity(it);
                        alert.dismiss();
                    }
                });
                alert.setControl2Listener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Passa qual controle a activity deve apresentar
                        it.putExtra("controle",2);
                        startActivity(it);
                        alert.dismiss();
                    }
                });
                alert.show();
            }
        });
    }



    private void searchDevices(){
        btAdapter.startDiscovery();
        progressBarSearch.setVisibility(View.VISIBLE);
    }



    private void createBroadcastReceiver(){
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(LTAG,"onReceive: " + intent.getAction());

                if (BluetoothDevice.ACTION_FOUND.equals(intent.getAction())){
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_NAME);
                    Log.i(LTAG, "Device found! " + device.getAddress());

                    devicesFoundNames.add(device.getName() + " - " + device.getAddress());
                    foundAdapter.notifyDataSetChanged();
                }

                if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(intent.getAction())){
                    progressBarSearch.setVisibility(View.GONE);
                }

                if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())){
                    getPairedDevices();
                }

            }
        };
    }


    /**
     * Obtém todos os dispositivos bluetooth que já foram pareados com o celular.
     */
    private void getPairedDevices(){
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();

        if (!pairedDevices.isEmpty()){
            for (BluetoothDevice device : pairedDevices){
                devicesPairedNames.add(device);
            }
        }
    }
}
