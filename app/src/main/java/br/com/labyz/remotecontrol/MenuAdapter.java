package br.com.labyz.remotecontrol;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by IT Staff on 05/07/2018.
 */

public class MenuAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<String> options;


    public MenuAdapter(Context context, ArrayList<String> options) {
        this.context = context;
        this.options = options;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return options.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String option = options.get(position);

        ViewHolder holder;

        if (convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.menu_row,parent,false);

            holder.tvName = convertView.findViewById(R.id.tvName);
            holder.ivProject = convertView.findViewById(R.id.ivProject);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }


        if (option.equals("Controle Robô SMARS")){
            holder.tvName.setText(option);
            holder.ivProject.setBackground(context.getResources().getDrawable(R.drawable.smars_small));
        }else if (option.equals("Sobre")){
            holder.tvName.setText(option);
            holder.ivProject.setImageDrawable(context.getResources().getDrawable(R.drawable.wisi));
        }

        return convertView;
    }


    public class ViewHolder{
        public TextView tvName = null;
        public ImageView ivProject = null;
    }


}
