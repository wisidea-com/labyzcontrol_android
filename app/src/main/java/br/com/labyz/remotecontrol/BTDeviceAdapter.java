package br.com.labyz.remotecontrol;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.labyz.remotecontrol.R;

/**
 * Created by IT Staff on 04/06/2018.
 */

public class BTDeviceAdapter extends ArrayAdapter<BluetoothDevice> {

    private Context context;
    private ArrayList<BluetoothDevice> btDevices;
    private LayoutInflater inflater;


    public BTDeviceAdapter(@NonNull Context context,@NonNull ArrayList<BluetoothDevice> objects) {
        super(context, 0, objects);

        this.context = context;
        this.btDevices = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        BluetoothDevice device = btDevices.get(position);

        ViewHolder holder;

        if (convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.devices_row,parent,false);

            holder.tvAddress = convertView.findViewById(R.id.tvAddress);
            holder.tvName = convertView.findViewById(R.id.tvName);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvName.setText(device.getName());
        holder.tvAddress.setText(device.getAddress());

        return convertView;
    }



    public class ViewHolder{
        public TextView tvName = null;
        public TextView tvAddress = null;
    }
}
