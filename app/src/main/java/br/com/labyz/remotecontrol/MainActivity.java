package br.com.labyz.remotecontrol;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.io.IOException;
import java.text.DecimalFormat;

import br.com.labyz.remotecontrol.R;

public class MainActivity extends AppCompatActivity implements JoystickListener,HorizontalJoystickListener,VerticalJoystickListener,CompoundButton.OnCheckedChangeListener, View.OnTouchListener, OnBluetoothReadListener, SeekBar.OnSeekBarChangeListener, View.OnClickListener {

    private final String LTAG = getClass().getSimpleName();

    private JoystickView joystick;
    private TextView tvAngulo;
    private TextView tvOffset;
    private TextView tvSonar;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btnOpened;
    private Button btnClosed;
    private Switch switch1;
    private Switch switch2;
    private Switch switch3;
    private SeekBar seekBar;
    private AlertConecting alertConecting;
    private AlertConectionError alertError;

    private BluetoothDevice device;
    private BluetoothComm comm;
    private JoystickBuffer jBuffer;

    private Boolean isConnected = false;
    private long    deBounce    = 0;

    private VerticalJoystickView vJoystick;
    private HorizontalJoystickView hJoystick;
    private SonarView sonarView;

    private float posX = 0;
    private float posY = 0;

    private int maxWidthHorizontal = 0;
    private int maxHeightVertical = 0;

    private int distance;

    //https://android-arsenal.com/details/1/2322

    //Botão e Chaves: B 00(id) 1(estado)
    //Slider: S 00 99
    //Joystick: J 0 +100(X) +100(Y) (0000)


    /**
     * Função para combinar os valores dos dois joysticks do controle 2 a cada atualização de posição
     * e formar um só comando.
     * @return retorna o comando.
     */
    public String getCombinedCmd() {
        Log.i("getCombinedCmd", "X Round: " + (Math.round(posX)) + ", Y Round: " + Math.round(posY));
        Log.i("getCombinedCmd", "WIDTH: " + hJoystick.myGetWidth() + ", HEIGHT:  " + vJoystick.myGetHeight());

        int newX = (Math.round(posX) * 99) / (maxWidthHorizontal/2);
        int newY = (Math.round(posY) * 99) / (maxHeightVertical/2);

        Log.i("getCombinedCmd", "NEW X: " + newX + ", NEW Y: " + newY);

        //J0-99099
        DecimalFormat format1 = new DecimalFormat("000");
        DecimalFormat format2 = new DecimalFormat("00");

        String sX = (newX >= 0) ? format1.format(newX) : format2.format(newX);
        String sY = (newY >= 0) ? format1.format(newY) : format2.format(newY);

        return "J0" + sX + sY;
    }


    protected void combineCommand(){
        String cmd = getCombinedCmd();
        Log.i("combineCommand", "cmd: [" + cmd + "]");

        jBuffer.put(cmd);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int controle = getIntent().getIntExtra("controle",1);

        //Verifica qual controle foi selecionado anteriormente
        if (controle == 1){
            setContentView(R.layout.activity_main);
            joystick = new JoystickView(this);

        }else if (controle == 2){
            setContentView(R.layout.activity_main_controle2);

            vJoystick = new VerticalJoystickView(this);
            hJoystick = new HorizontalJoystickView(this);
        }

        (btn1 = findViewById(R.id.btn1)).setOnTouchListener(this);
        (btn2 = findViewById(R.id.btn2)).setOnTouchListener(this);
        (btn3 = findViewById(R.id.btn3)).setOnTouchListener(this);
        (btnOpened = findViewById(R.id.btnOpened)).setOnClickListener(this);
        (btnClosed = findViewById(R.id.btnClosed)).setOnClickListener(this);
        (switch1 = findViewById(R.id.switch1)).setOnCheckedChangeListener(this);
        (switch2 = findViewById(R.id.switch2)).setOnCheckedChangeListener(this);
        (switch3 = findViewById(R.id.switch3)).setOnCheckedChangeListener(this);
        (seekBar = findViewById(R.id.seekBar)).setOnSeekBarChangeListener(this);
        tvAngulo = findViewById(R.id.tvAngulo);
        tvOffset = findViewById(R.id.tvOffset);
        sonarView = findViewById(R.id.sonarView);
        tvSonar = findViewById(R.id.tvSonar);

        tvSonar.setText("Sonar OFF");

        device = getIntent().getParcelableExtra("extra_device");

        tvAngulo.setVisibility(View.INVISIBLE);
        tvOffset.setVisibility(View.INVISIBLE);

        alertConecting = new AlertConecting(this);
    }



    @Override
    protected void onStart() {
        super.onStart();

        alertConecting.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                comm = new BluetoothComm(device, MainActivity.this);

                try {
                    comm.connect();
                    jBuffer = new JoystickBuffer(100).startThread();
                    isConnected = true;
                    alertConecting.dismiss();
                } catch (IOException e) {
                    isConnected = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(MainActivity.this,"Erro ao conectar!", Toast.LENGTH_LONG).show();
                            alertConecting.dismiss();
                            showAlertError();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();
    }



    @Override
    protected void onStop() {
        if (isConnected){
            try {
                jBuffer.finish();
                comm.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        super.onStop();
    }



    private void showAlertError(){
        alertError = new AlertConectionError(this);
        alertError.setOnBackListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                alertError.dismiss();
            }
        });
        alertError.show();
    }



    //Square
    @Override
    public void onDrag(float X, float Y, int maxWidth, int maxHeight) {
        if (System.currentTimeMillis() > deBounce) {
            tvAngulo.setText("" + X);
            tvOffset.setText("" + Y);

            Log.i("onDrag", "Width: [" + maxWidth+ "], Height: [" +maxHeight + "]");

            int newX = (Math.round(X) * 99) / (maxWidth/2);
            int newY = (Math.round(Y) * 99) / (maxHeight/2);

            Log.i("onDrag", "NEW X: " + newX + ", NEW Y: " + newY);

            //J0-99099
            DecimalFormat format1 = new DecimalFormat("000");
            DecimalFormat format2 = new DecimalFormat("00");

            String sX = (newX >= 0) ? format1.format(newX) : format2.format(newX);
            String sY = (newY >= 0) ? format1.format(newY) : format2.format(newY);

            String cmd = "J0" + sX + sY;
            Log.i("onDrag", "cmd: [" + cmd + "]");

            jBuffer.put(cmd);
        }
    }


    //Vertical
    @Override
    public void onDragY(float Y, int maxWidth, int maxHeight) {
        Log.i("onDragY", "Valor: [" + Y + "]");

        tvOffset.setText("" + Y);
        posY = Y;

        combineCommand();
    }


    //Horizontal
    @Override
    public void onDragX(float X, int maxWidth, int maxHeight) {
        Log.i("onDragX", "Valor: [" + X + "]");
        tvAngulo.setText("" + X);
        posX = X;

        combineCommand();
    }


    //Vertical
    @Override
    public void onUpY(int maxWidth, int maxHeight) {
        posY = 0.0f;
        jBuffer.clear();
        sendCommand(getCombinedCmd());
        tvAngulo.setText("0");
        tvOffset.setText("0");
        deBounce = System.currentTimeMillis() + 100;
    }


    //Horizontal
    @Override
    public void onUpX(int maxWidth, int maxHeight) {
        posX = 0.0f;
        jBuffer.clear();
        sendCommand(getCombinedCmd());
        tvAngulo.setText("0");
        tvOffset.setText("0");
        deBounce = System.currentTimeMillis() + 100;
    }



    //Square
    @Override
    public void onUp() {
        jBuffer.clear();
        sendCommand("J0000000");
        tvAngulo.setText("0");
        tvOffset.setText("0");
        deBounce = System.currentTimeMillis() + 100;
    }



    @Override
    public void onSetudDimensionsY(int maxWidth, int maxHeight) {
        maxHeightVertical = maxHeight;
    }



    @Override
    public void onSetudDimensionsX(int maxWidth, int maxHeight) {
        maxWidthHorizontal = maxWidth;
    }



    //Listener Switch
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch(buttonView.getId()){
            case R.id.switch1:
                if (isChecked){
                    sendCommand("B051");
                }else {
                    sendCommand("B050");
                }
                break;

            case R.id.switch2:
                if (isChecked){
                    sendCommand("B041");
                }else {
                    sendCommand("B040");
                }
                break;

            case R.id.switch3:
                if (isChecked){
                    sendCommand("B031");
                }else {
                    sendCommand("B030");
                }
                break;
        }
    }



    private void sendCommand(String command){
        Log.i(LTAG,"Comando enviado: " + command);

        comm.send(command);
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {

        String cmd = "";

        switch(v.getId()){
            case R.id.btn1:
                cmd = "B00";
                break;

            case R.id.btn2:
                cmd = "B01";
                break;

            case R.id.btn3:
                cmd = "B02";
                break;
        }


        //Envia o comando ao pressionar e soltar o botão.
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                cmd += "1";
                sendCommand(cmd);
                break;

            case MotionEvent.ACTION_UP:
                cmd += "0";
                sendCommand(cmd);
                break;
        }

        return false;
    }



    //BT Listener
    //Recebe o comando enviado pelo arduino e envia para a view do sonar.
    @Override
    public void onSonarRead(String cmd) {
        String split[] = cmd.split(",");
        //String angle = split[1];
        distance = Integer.parseInt(split[2]);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvSonar.setText(distance+"cm");
            }
        });

        Log.i(LTAG,"onSonarRead  sonarViewHeiht: ["+sonarView.getHeight()+"]");
        int newDistance = (distance * sonarView.getHeight()) / 30;

        sonarView.updateDistance(newDistance);
    }



    //Eventos da seekbar
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        String cmd = "J1";

        DecimalFormat format1 = new DecimalFormat("000");
        cmd = cmd + format1.format(progress+60) + "000";

        jBuffer.put(cmd);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}


    //Eventos botões aberto e fechado
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnOpened: jBuffer.put("J1060000"); break;
            case R.id.btnClosed: jBuffer.put("J1180000"); break;
        }
    }


    /**
     * Buffer para enviar os comandos.
     * É possível setar um delay entre o envio de cada comando.
     * Necessário para o Arduino poder processar cada comando corretamente.
     */
    public class JoystickBuffer extends Thread {
        protected boolean   isRunning   = false;
        protected String    buffer      = null;
        protected int       minInterval = 0;


        public JoystickBuffer(int minInterval) {
            super();
            this.minInterval = minInterval;
        }

        public JoystickBuffer startThread() {
            this.start();
            return this;
        }

        public void finish() {
            isRunning =false;
            this.interrupt();
        }


        public void put(String cmd) {
            synchronized (this) {
                buffer = cmd;
                this.notifyAll();
            }
        }

        public void run() {
            String cmd;
            long last = 0;
            isRunning = true;


            while (isRunning) {
                cmd = null;

                synchronized (this) {
                    if (buffer != null && System.currentTimeMillis() > last + minInterval) {
                        cmd     = buffer;
                        buffer  = null;
                    } else {
                        try {
                            if (buffer == null) {
                                this.wait();
                            } else {
                                long mInterval = minInterval - (System.currentTimeMillis() - last);
                                this.wait(mInterval > 0 ? mInterval: 0);
                            }
                        } catch (InterruptedException ie) {}
                    }
                }

                if (cmd != null) {
                    sendCommand(cmd);
                    last = System.currentTimeMillis();
                }
            }

            Log.w("JoystickBuffer","Thread Finished");
        }

        public void clear() {
            synchronized (this) {
                buffer = null;
            }
        }
    }
}
