package br.com.labyz.remotecontrol;

/**
 * Created by IT Staff on 28/05/2018.
 */

public interface HorizontalJoystickListener {
    void onDragX(float X, int maxWidth, int maxHeight);
    void onUpX(int maxWidth, int maxHeight);
    void onSetudDimensionsX(int maxWidth, int maxHeight);
}
