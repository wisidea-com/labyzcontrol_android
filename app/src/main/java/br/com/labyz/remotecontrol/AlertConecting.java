package br.com.labyz.remotecontrol;

import android.app.Dialog;
import android.content.Context;
import android.widget.ProgressBar;

import br.com.labyz.remotecontrol.R;
/**
 * Created by IT Staff on 30/05/2018.
 */

public class AlertConecting {

    private ProgressBar progressBar;
    private Context context;
    private Dialog dialog;





    public AlertConecting(Context context) {
        this.context = context;
        init();
    }



    private void init(){
        dialog = new Dialog(context);
        dialog.getWindow().requestFeature(1);
        dialog.setContentView(R.layout.alert_loading);

        progressBar = dialog.findViewById(R.id.progressBar);
        dialog.setCancelable(false);
    }



    public void show(){
        dialog.show();
    }



    public void dismiss(){
        dialog.dismiss();
    }

}
